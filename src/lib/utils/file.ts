export function downloadToFile(contentAsString: string, filename: string, contentType: string) {
	const link = document.createElement('a');
	const file = new Blob([contentAsString], { type: contentType });

	link.href = URL.createObjectURL(file);
	link.download = filename;
	link.style.display = 'none';
	document.body.appendChild(link);
	link.click();

	URL.revokeObjectURL(link.href);
	document.body.removeChild(link);
}
