import RLP from 'rlp';
import type { BehaviorRequest } from '../EmcJsonRpcProvider';
import { Signature, hexlify } from 'ethers';

export function encodeBehavior(bx: BehaviorRequest): string {
	// Set signature params (v,r,s) to 0 before signature
	// https://medium.com/mycrypto/the-magic-of-digital-signatures-on-ethereum-98fe184dc9c7
	let encoded = RLP.encode([bx.chainId, bx.quantity, bx.timestamp, bx.input, 0, 0]);
	return hexlify(encoded);
}

export function encodeSignedBehavior(bx: BehaviorRequest, signature: Signature): string {
	let encoded = RLP.encode([
		bx.chainId,
		bx.quantity,
		bx.timestamp,
		bx.input,
		signature.v,
		signature.r,
		signature.s
	]);
	return hexlify(encoded);
}
