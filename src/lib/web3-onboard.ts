import { browser } from '$app/environment';
import Onboard from '@web3-onboard/core';
import type { OnboardAPI } from '@web3-onboard/core';
import injectedWalletsModule from '@web3-onboard/injected-wallets';
// import walletConnectModule from '@web3-onboard/walletconnect';

import logo from '$lib/assets/logo.svg';

let onboard;

if (browser) {
	const injected = injectedWalletsModule();
	// const walletConnect = walletConnectModule({
	// 	connectFirstChainId: true,
	// 	qrcodeModalOptions: {
	// 		mobileLinks: ['rainbow', 'metamask', 'argent', 'trust', 'imtoken', 'pillar']
	// 	}
	// });

	const wallets = [injected];

	const chains = [
		{
			id: 6363,
			token: 'ECOM',
			label: 'Clermont (testnet)',
			color: '#3b82f6',
			rpcUrl: 'http://ecomobicoin-1.cri.local.isima.fr:8545/'
			// blockExplorerUrl
		},
		{
			id: 636363,
			token: 'LECOM',
			label: 'Local testnet',
			rpcUrl: 'http://127.0.0.1:8545/'
		}
	];

	const appMetadata = {
		name: 'EcoMobiCoin Example App',
		icon: logo,
		explore: 'https://ecomobicoin.limos.fr/',
		// logo: '<svg />',
		description: 'Interact with the EcoMobiCoin blockchain',
		recommendedInjectedWallets: [{ name: 'MetaMask', url: 'https://metamask.io' }]
	};

	if (!onboard) {
		onboard = Onboard({
			wallets,
			chains,
			appMetadata
		});
	}
}

export default onboard as OnboardAPI;
