import {
	assertArgument,
	getBigInt,
	hexlify,
	JsonRpcProvider,
	resolveAddress,
	toQuantity,
	TransactionResponse,
	type AddressLike,
	type BigNumberish,
	type JsonRpcTransactionRequest,
	type TransactionRequest
} from 'ethers';

export interface BehaviorRequest {
	type?: null | number;

	from?: null | AddressLike;

	timestamp?: null | number;
	// nonce?: null | number;

	input?: null | string;
	quantity?: null | BigNumberish;
	chainId?: null | BigNumberish;

	customData?: any;
}

export interface JsonRpcBehaviorRequest {
	// from?: string;
	timestamp?: string;
	quantity?: string;

	chainId?: string;
	type?: string;

	// nonce?: string;
	input?: string;
}

class EmcJsonRpcProvider extends JsonRpcProvider {
	// async _perform(req: PerformActionRequest): Promise<any> {
	// 	// Legacy networks do not like the type field being passed along (which
	// 	// is fair), so we delete type if it is 0 and a non-EIP-1559 network
	// 	if (req.method === 'call' || req.method === 'estimateGas') {
	// 		let tx = req.transaction;
	// 		if (tx && tx.type != null && getBigInt(tx.type)) {
	// 			// If there are no EIP-1559 properties, it might be non-EIP-a559
	// 			if (tx.maxFeePerGas == null && tx.maxPriorityFeePerGas == null) {
	// 				const feeData = await this.getFeeData();
	// 				if (feeData.maxFeePerGas == null && feeData.maxPriorityFeePerGas == null) {
	// 					// Network doesn't know about EIP-1559 (and hence type)
	// 					req = Object.assign({}, req, {
	// 						transaction: Object.assign({}, tx, { type: undefined })
	// 					});
	// 				}
	// 			}
	// 		}
	// 	}
	// 	const request = this.getRpcRequest(req);

	// 	if (request != null) {
	// 		return await this.send(request.method, request.args);
	// 	}
	// 	if (req.method.startsWith('emc_')) {
	// 		switch (req.method) {
	// 			case 'getGasPrice':
	// 				return { method: 'emc_gasPrice', args: [] };
	// 		}
	// 		return null;
	// 	} else {
	// 		return super._perform(req);
	// 	}
	// }

	trieEncodeTransaction(): string {
		return '';
	}

	trieEncodeBehavior(): string {
		return '';
	}

	getRpcTransaction(tx: TransactionRequest): JsonRpcTransactionRequest {
		return super.getRpcTransaction(tx);
	}

	/**
	 *  Returns %%tx%% as a normalized JSON-RPC transaction request,
	 *  which has all values hexlified and any numeric values converted
	 *  to Quantity values.
	 */
	getRpcBehavior(tx: BehaviorRequest): JsonRpcBehaviorRequest {
		const result: JsonRpcBehaviorRequest = {};

		// JSON-RPC now requires numeric values to be "quantity" values
		[
			'chainId',
			'gasLimit',
			'gasPrice',
			'type',
			'maxFeePerGas',
			'maxPriorityFeePerGas',
			'nonce',
			'value'
		].forEach((key) => {
			if ((<any>tx)[key] == null) {
				return;
			}
			let dstKey = key;
			if (key === 'gasLimit') {
				dstKey = 'gas';
			}
			(<any>result)[dstKey] = toQuantity(getBigInt((<any>tx)[key], `tx.${key}`));
		});

		// Make sure addresses and data are lowercase
		['from', 'to', 'data'].forEach((key) => {
			if ((<any>tx)[key] == null) {
				return;
			}
			(<any>result)[key] = hexlify((<any>tx)[key]);
		});

		// Normalize the access list object
		// if (tx.accessList) {
		// 	result['accessList'] = accessListify(tx.accessList);
		// }

		return result;
	}

	// async getBehavior(hash: string): Promise<null | TransactionResponse> {
	//     const { network, params } = await resolveProperties({
	//         network: this.getNetwork(),
	//         params: this.#perform({ method: "getTransaction", hash })
	//     });
	//     if (params == null) { return null; }

	//     return this._wrapTransactionResponse(params, network);
	// }

	// async sendBehavior(bx: BehaviorRequest): Promise<TransactionResponse> {
	// 	// This cannot be mined any earlier than any recent block
	// 	const blockNumber = await this.provider.getBlockNumber();

	// 	// Send the transaction
	// 	// const hash = await this.sendUncheckedBehavior(bx);

	// 	// Unfortunately, JSON-RPC only provides and opaque transaction hash
	// 	// for a response, and we need the actual transaction, so we poll
	// 	// for it; it should show up very quickly
	// 	return await new Promise((resolve, reject) => {
	// 		const timeouts = [1000, 100];
	// 		const checkBx = async () => {
	// 			// Try getting the transaction
	// 			const tx = await this.provider.getTransaction(hash);
	// 			if (tx != null) {
	// 				resolve(tx.replaceableTransaction(blockNumber));
	// 				return;
	// 			}

	// 			// Wait another 4 seconds
	// 			this.provider._setTimeout(() => {
	// 				checkBx();
	// 			}, timeouts.pop() || 4000);
	// 		};
	// 		checkBx();
	// 	});
	// }
}
